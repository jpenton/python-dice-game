from random import *
from tkinter import *


class Dice:
    def __init__(self):
        self.window_main = Tk()
        self.image1 = PhotoImage(file="assets/1.gif")
        self.label1 = Label(master=self.window_main, image=self.image1)
        self.label2 = Label(master=self.window_main, image=self.image1)
        self.total_var = StringVar(value="Total = 2")
        self.label_total = Label(master=self.window_main, textvariable=self.total_var)
        self.button_roll = Button(
            master=self.window_main, text="Roll", command=self.roll_dice
        )
        self.button_reset = Button(
            master=self.window_main, text="Reset", command=self.reset_game
        )

        self.label1.grid(row=0, column=0)
        self.label2.grid(row=0, column=1)
        self.label_total.grid(row=1, column=0, columnspan=2)
        self.button_roll.grid(row=2, column=0)
        self.button_reset.grid(row=2, column=1)

        mainloop()

    def reset_game(self):
        self.image1 = PhotoImage(file="assets/1.gif")

        self.total_var.set("Total = 2")
        self.label1.config(image=self.image1)
        self.label2.config(image=self.image1)

    def roll_dice(self):
        num1 = randint(1, 6)
        num2 = randint(1, 6)
        self.image1 = PhotoImage(file="assets/{}.gif".format(num1))
        self.image2 = PhotoImage(file="assets/{}.gif".format(num2))

        self.total_var.set("Total = {}".format(num1 + num2))
        self.label1.config(image=self.image1)
        self.label2.config(image=self.image2)


game = Dice()
